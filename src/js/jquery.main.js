jQuery(function() {
    initSlickCarousel();
    initMobileNav();
    initPopups();
});


// slick init
function initSlickCarousel() {
    jQuery('.step-carousel').slick({
        slidesToScroll: 1,
        rows: 0,
        prevArrow: '<button class="slick-prev"></button>',
        nextArrow: '<button class="slick-next"></button>',
        infinite: true,
        centerMode: true,
        centerPadding: '0px',
        pauseOnHover: false,
        // variableWidth: true,
        asNavFor: '.step-nav'
    });
    jQuery('.step-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        focusOnSelect: true,
        infinite: true,
        centerMode: true,
        centerPadding: '0px',
        pauseOnHover: false,
        asNavFor: '.step-carousel'
    });
    jQuery('.testimonials-slider').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        rows: 0,
        prevArrow: '<button class="slick-prev"></button>',
        nextArrow: '<button class="slick-next"></button>',
        dots: true,
        dotsClass: 'slick-dots',
        infinite: true,
        variableWidth: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToScroll: 1,
                slidesToShow: 1,
                centerMode: true,
                variableWidth: false,
                centerPadding: '0px'
            }
        },
        {
            breakpoint: 1360,
            settings: {
                slidesToScroll: 1,
                slidesToShow: 2,
                centerMode: true,
                variableWidth: false,
                centerPadding: '0px'
            }
        }]
    });
}

// mobile menu init
function initMobileNav() {
    jQuery('body').mobileNav({
        menuActiveClass: 'nav-active',
        menuOpener: '.nav-opener'
    });
}

// popups init
function initPopups() {
    jQuery('.popup-holder').contentPopup({
        mode: 'click',
        btnOpen: '.opener'
    });
}
